/*
 * Al_Prim.cpp
 *
 *  Created on: 15 abr. 2018
 *      Author: Bryan
 */

#include<bits/stdc++.h>
using namespace std;
# define INF 0x3f3f3f3f

// iPair ==>  Integer Pair
typedef pair<int, int> iPair;

// This class represents a directed graph using
// adjacency list representation
class Graph
{
    int V;    // No. of vertices

    // In a weighted graph, we need to store vertex
    // and weight pair for every edge
    list< pair<int, int> > *adj;

public:
    Graph(int V);  // Constructor

    // function to add an edge to graph
    void addEdge(int u, int v, int w);

    // Print MST using Prim's algorithm
    void primMST();
};

// Allocates memory for adjacency list
Graph::Graph(int V)
{
    this->V = V;
    adj = new list<iPair> [V];
}

void Graph::addEdge(int u, int v, int w)
{
    adj[u].push_back(make_pair(v, w));
    adj[v].push_back(make_pair(u, w));
}

// Prints shortest paths from src to all other vertices
void Graph::primMST()
{
    // Create a priority queue to store vertices that
    // are being preinMST. This is weird syntax in C++.
    // Refer below link for details of this syntax
    // http://geeksquiz.com/implement-min-heap-using-stl/
    priority_queue< iPair, vector <iPair> , greater<iPair> > pq;

    int src = 0; // Taking vertex 0 as source

    // Create a vector for keys and initialize all
    // keys as infinite (INF)
    vector<int> key(V, INF);

    // To store parent array which in turn store MST
    vector<int> parent(V, -1);

    // To keep track of vertices included in MST
    vector<bool> inMST(V, false);

    // Insert source itself in priority queue and initialize
    // its key as 0.
    pq.push(make_pair(0, src));
    key[src] = 0;

    /* Looping till priority queue becomes empty */
    while (!pq.empty())
    {
        // The first vertex in pair is the minimum key
        // vertex, extract it from priority queue.
        // vertex label is stored in second of pair (it
        // has to be done this way to keep the vertices
        // sorted key (key must be first item
        // in pair)
        int u = pq.top().second;
        pq.pop();

        inMST[u] = true;  // Include vertex in MST

        // 'i' is used to get all adjacent vertices of a vertex
        list< pair<int, int> >::iterator i;
        for (i = adj[u].begin(); i != adj[u].end(); ++i)
        {
            // Get vertex label and weight of current adjacent
            // of u.
            int v = (*i).first;
            int weight = (*i).second;

            //  If v is not in MST and weight of (u,v) is smaller
            // than current key of v
            if (inMST[v] == false && key[v] > weight)
            {
                // Updating key of v
                key[v] = weight;
                pq.push(make_pair(key[v], v));
                parent[v] = u;
            }
        }
    }

    // Print edges of MST using parent array
    for (int i = 1; i < V; ++i)
        printf("%d - %d\n", parent[i], i);
}

// Driver program to test methods of graph class
void al_prim()
{
    // create the graph given in above fugure
    int V;

 	cout<<"                   A L G O R I T M O   D E   P R I M "<<endl;
 	cout<<"Vertices: ";
 	cin>>V;
 	cout<<endl;
 	Graph g(V);
 	int a1=0,a2=1,a3=4,b1=0,b2=7,b3=8,c1=1,c2=2,c3=8,d1=1,d2=7,d3=11,e1=2,e2=3,e3=7,f1=2,f2=8,f3=2,g1=2,g2=5,g3=4,h1=3,h2=4,h3=9;
 	int i1=3,i2=5,i3=14,j1=4,j2=5,j3=10,k1=5,k2=6,k3=2,l1=6,l2=7,l3=1,m1=6,m2=8,m3=6,n1=7,n2=8,n3=7;
    //  making above shown graph

	cout<<"Grafo V:"<<endl;
	cout<<a1<<" - "<<a2<<" - "<<a3<<endl;
    cout<<b1<<" - "<<b2<<" - "<<b3<<endl;
    cout<<c1<<" - "<<c2<<" - "<<c3<<endl;
    cout<<d1<<" - "<<d2<<" - "<<d3<<endl;
    cout<<e1<<" - "<<e2<<" - "<<e3<<endl;
    cout<<f1<<" - "<<f2<<" - "<<f3<<endl;
    cout<<g1<<" - "<<g2<<" - "<<g3<<endl;
    cout<<h1<<" - "<<h2<<" - "<<h3<<endl;
    cout<<i1<<" - "<<i2<<" - "<<i3<<endl;
    cout<<j1<<" - "<<j2<<" - "<<j3<<endl;
    cout<<k1<<" - "<<k2<<" - "<<k3<<endl;
    cout<<l1<<" - "<<l2<<" - "<<l3<<endl;
    cout<<m1<<" - "<<m2<<" - "<<m3<<endl;
    cout<<n1<<" - "<<n2<<" - "<<n3<<endl;


    g.addEdge(a1,a2,a3);

    g.addEdge(b1,b2,b3);

    g.addEdge(c1,c2,c3);

    g.addEdge(d1,d2,d3);

    g.addEdge(e1,e2,e3);

    g.addEdge(f1,f2,f3);

    g.addEdge(g1,g2,g3);

    g.addEdge(h1,h2,h3);

    g.addEdge(i1,i2,i3);

    g.addEdge(j1,j2,j3);

    g.addEdge(k1,k2,k3);

    g.addEdge(l1,l2,l3);

    g.addEdge(m1,m2,m3);

    g.addEdge(n1,n2,n3);

 	cout<<endl;
 	cout<<"Resultado:"<<endl;
    g.primMST();


}
