/*
 * Utiliarios.cpp
 *
 *  Created on: 17 abr. 2018
 *      Author: Windows
 */
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <chrono>
#include <vector>
#include <fstream>
#include <cstring>
#include <cmath>
#include "../Al_Prim/Al_Prim.hpp"
#include "../Boyer_Moore/Boyer_Moore.hpp"
#include "../Criba_Eratostenes/Criba_Eratostenes.hpp"
#include "../Heapsort/Heapsort.hpp"

using namespace std;
using namespace std::chrono;


	void linea() {
		cout<<"---------------------------"<<endl;
	}

	int verificarcliente(struct form_cliente cliente[],struct form_cliente aux,int cant_cliente) {
		//0 : procede
		//1 : no procede
		int x=0;
		for (int i=0;i<cant_cliente;i++) {
			if (cliente[i].nombre==aux.nombre ||
				cliente[i].apellido==aux.apellido ||
				cliente[i].id==aux.id ||
				cliente[i].dni==aux.dni ||
				cliente[i].nac==aux.nac ||
				cliente[i].celular==aux.celular ||
				cliente[i].email==aux.email) {
				x=1;
			}
		}
		return x;
	}

	int verificarvendedor(struct form_vendedor cliente[],struct form_vendedor aux,int cant_vendedor) {
		//0 : procede
		//1 : no procede
		int x=0;
		for (int i=0;i<cant_vendedor;i++) {
			if (cliente[i].nombre==aux.nombre ||
				cliente[i].apellido==aux.apellido ||
				cliente[i].id==aux.id ||
				cliente[i].dni==aux.dni ||
				cliente[i].nac==aux.nac ||
				cliente[i].celular==aux.celular ||
				cliente[i].email==aux.email ||
				cliente[i].ruc==aux.ruc) {
				x=1;
			}
		}
		return x;
	}

	int numcifras(int num) {
		int x=0;
		int aux=num;

		while (aux>0) {
			aux/=10;
			x++;
		}

		return x;
	}

	void mostrar(struct producto p[],int cant_producto) {
		cout<<""<<endl;
		cout<<"Cantidad";
		cout.width(10);
		cout<<"Precio";
		cout.width(15);
		cout<<"Nombre"<<endl;
		cout<<"--------------------------------------------"<<endl;

		for (int i=0;i<cant_producto;i++) {

			cout<<p[i].cantidad;
			for (int j=1;j<13-numcifras(p[i].cantidad);j++) {
				cout<<" ";
			}
			cout<<p[i].precio;
			for (int j=1;j<16-numcifras(p[i].precio);j++) {
					cout<<" ";
			}
			cout<<p[i].nombre<<endl;
		}
	}

	void heapifyarreglo(int arreglo[],int i,int cant) {
	//ordenar subarbol con nodo raiz i

		int padre=i;
		int izq=2*i+1;
		int der=2*i+2;

		if (izq<cant && arreglo[izq]>arreglo[padre]) {
			padre=izq;
		}

		if (der<cant && arreglo[der]>arreglo[padre]) {
			padre=der;
		}

		if (padre!=i) {
			swap(arreglo[padre],arreglo[i]);
			heapifyarreglo(arreglo,padre,cant);
		}
	}

	void heapsortarreglo(int arreglo[],int cant) {
		//orden: (1)descendente (otro valor)ascendente

		for (int i=cant/2-1;i>=0;i--) {
			//formar monticulo
			//cant/2-1:cant de nodos padres

			heapifyarreglo(arreglo,i,cant);
		}

		for (int i=cant-1;i>=0;i--) {
			//swap ultimo y primer elemento
			//heapify sin el ultimo elemento

			swap(arreglo[i],arreglo[0]);
			heapifyarreglo(arreglo,0,i);
		}
	}

	void boyer_moore_proy(struct producto p[], struct producto b[],int cant_producto){
		int m = 0;
		int extension =0;
		char cadena[100];
		char pedido[100];

		for(int i=1;i<=cant_producto;i++){
			extension=p[i].nombre.size();
		}
		/*
		pedido=p[100].nombre;
		*/


		do {
			cout<<"Ingrese la palabra a buscar:"<<endl;
			cin>>cadena;
			m = strlen(cadena);
			//m = cin.getline(cadena, 100, '\n');
			if (m > extension)
				cout << "\t\t ERROR!!! Texto > Patron... \n";
		} while (m > extension);
		boyer_moore_proyecto(pedido, cadena, m, extension);
		for(int i=1;i<=cant_producto;i++){
				if(cadena == p[i].nombre){
					b[i].nombre=p[i].nombre;
					b[i].cantidad=p[i].cantidad;
					b[i].precio=p[i].precio;
					}
			}
	}

	void algoritmos(){
		system("CLS");
		cout<<"*********ALGORITMOS*********"<<endl;
		int opcion;
		cout<<"<1> La criba de Eratostenes"<<endl;
		cout<<"<2> Algoritmo de PRIM"<<endl;
		cout<<"<3> Algoritmo de Boyer-Moore"<<endl;
		cout<<"<4> Algorimo de Heapsort"<<endl;
		cout<<"<5> Salir"<<endl;
		cin>>opcion;

		switch(opcion){
			case 1:
				inicio_criba();
				break;

			case 2:
				system("cls");
							//inicio prim
							al_prim();

				break;

			case 3:
				boyer_moore();
				break;

			case 4:
				system("cls");
				//inicio heapsort
				cout<<"**********HEAPSORT**********"<<endl;
				cout<<"Ingresar Cantidad de Datos"<<endl;
				int cant; cin>>cant;
				int arreglo[cant];

				int x;
				cout<<"<1> Datos Iguales"<<endl;
				cout<<"<2> Datos Aleatorios"<<endl;
				cin>>x;

				for (int i=0;i<cant;i++) {
					if (x==1) {
						arreglo[i]=7;
					}
					else if (x==2) {
						arreglo[i]=rand()%1001;
					}
				}

				auto inicio = high_resolution_clock::now();
	
				heapsortarreglo(arreglo,cant);

				for (int i=0;i<cant;i++) {
					if (i==cant-1) {
						cout<<arreglo[i];
					}
					else {
						cout<<arreglo[i]<<",";
					}
				}

				auto fin = high_resolution_clock::now();

				auto duracion = duration_cast<microseconds>(fin-inicio);

				cout<<""<<endl;
				cout<<"El Tiempo de Ejecucion fue: "<<duracion.count()<<endl;

				//fin heapsort
				break;
/*
			default:
				exit(0);
*/
		}
	}

long long primo(int n){
	long long pri;
	vector<long> cod;
	vector<bool> primo;
	bool aux=false;
		for (int i=0;i<n;i++){
			primo.push_back(aux);
		}
		for (int i=0;i<n;i++){
			primo[i]=true;
		}
		for(int i=2;i<pow(n,0.5);i++){
			if(primo[i]){
			for(int j=2;i*j<n;j++){
				primo[i*j]=false;
				}
			}
		}
		for(int i=2;i<n;i++){
			if(primo[i]){
				cod.push_back(i);
			}
		}
		pri=cod[cod.size()-1];
	return pri;
}


long long contrasena(long long a,long long b){
	long long contra;
	contra=primo(a)*primo(b);
	return contra;
}
