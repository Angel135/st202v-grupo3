/*
 * Utilitarios.hpp
 *
 *  Created on: 17 abr. 2018
 *      Author: Windows
 */

#ifndef UTILITARIOS_UTILITARIOS_HPP_
#define UTILITARIOS_UTILITARIOS_HPP_
#include <iostream>
using namespace std;

long long contrasena(long long a,long long b);

	struct producto {
		string nombre;
		int cantidad;
		float precio;
	};

	struct form_vendedor {
		string nombre;
		string apellido;
		int id;
		long long dni;
		int nac;
		string direccion;
		long long celular;
		string email;
		int ruc;
		long long psw;
	};

	struct form_cliente {
		string nombre;
		string apellido;
		int id;
		long long dni;
		int nac;
		string direccion;
		long long celular;
		string email;
		long long psw;
	};

	int numcifras(int num);
	void mostrar(struct producto p[],int cant_producto);
	int verificarcliente(struct form_cliente cliente[],struct form_cliente aux,int cant_cliente);
	int verificarvendedor(struct form_vendedor cliente[],struct form_vendedor aux,int cant_vendedor);
	void mostrar(struct producto p[],int cant_producto);
	void leer_archivo();
    void heapifyarreglo(int arreglo[],int i,int cant);
    void heapsortarreglo(int arreglo[],int cant);
	void algoritmos();
	void linea();
	void boyer_moore_proy(struct producto p[], struct producto b[], int cant_producto);


#endif /* UTILITARIOS_UTILITARIOS_HPP_ */
