/*
 * Boyer_Moore.hpp
 *
 *  Created on: 15 abr. 2018
 *      Author: Bryan
 */

#ifndef BOYER_MOORE_BOYER_MOORE_HPP_
#define BOYER_MOORE_BOYER_MOORE_HPP_

#include <iostream>
#include <cstring>
using namespace std;
//int boyer_moore_productos(char t[], char p[], int M, int N);
void boyer_moore();
int boyer_moore_proyecto(string t, char p[], int m, int longitud);

#endif /* BOYER_MOORE_BOYER_MOORE_HPP_ */
