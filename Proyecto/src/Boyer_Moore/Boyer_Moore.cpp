#include <cstdlib>
#include <iostream>
#include <cstring>
#include <chrono>
#include"boyer_moore.hpp"
#include <fstream>
#include "../Utilitarios/Utilitarios.hpp"
#define MAX(A,B) (A>B) ? A : B ;
using namespace std;
using namespace std::chrono;
int longitud;

void prebm(char p[], int delta[], int M, int longitud) {
	for (int i = 0; i <= 255; i++)
		delta[i] = M;
	for (int i = 0; i < M; i++)
		delta[p[i]] = M - 1 - i;
}
int boyer_moore_matcher(char t[], char p[], int M, int N) {
	int i = M - 1, j = M - 1;
	int delta[256];

	prebm(p, delta,M,N);

	while (j >= 0) {
		while (t[i] != p[j]) {

			i += MAX(M - j, delta[t[i]])
			;

			if (i >= N)
				return N;
			j = M - 1;
		}
		j--;
		i--;

		if (j < 0) {
			cout << "\n\n\t\t Encontrado en : " << i + 1;
			i += M + 1;
			j = M - 1;
		}
	}
	return 0;
}

int boyer_moore_productos(string t, char p[], int M, int longitud) {
	int i = M - 1, j = M - 1;
	int delta[256];

	prebm(p, delta, M, longitud);

	while (j >= 0) {
		while (t[i] != p[j]) {

			i += MAX(M - j, delta[t[i]])
			;

			if (i >= longitud){
				return longitud;
			}

			j = M - 1;
		}
		j--;
		i--;
		if (j < 0) {
						cout << "\n\n\t\t Se encontro el producto pedido";
						i += M + 1;
						j = M - 1;
					}
	}
	cout << "No funciona";
	return 0;
}

void prebmproyecto(char p[], int delta[], int m, int longitud) {
	for (int i = 0; i <= 255; i++)
		delta[i] = m;
	for (int i = 0; i < m; i++)
		delta[p[i]] = m - 1 - i;
}
int boyer_moore_proyecto(string t, char p[], int m, int longitud) {
	int i = m - 1, j = m - 1;
	int delta[256];
	int acumulador=0;

	prebmproyecto(p, delta, m, longitud);

	while (j >= 0) {
		while (t[i] != p[j]) {

			i += MAX(m - j, delta[t[i]])
			;

			if (i >= longitud){
				return longitud;
			}

			j = m - 1;
		}
		j--;
		i--;
		if (j < 0) {
						acumulador=acumulador+1;
						//cout << "\n\n\t\t Se encontro el producto pedido";
						i += m + 1;
						j = m - 1;

					}
		cout<<"Se encontraron "<<acumulador<< " coincidencias.";
	}
	cout << "No funciona";





	return 0;
}

void escribir() {
	ofstream archivo;

	archivo.open("prueba.txt", ios::out);

	if (archivo.fail()) {
		cout << "No se pudo crear archivo";
		exit(1);
	}

	archivo
			<< "computadora laptop mouse impresora monitor teclado disco duro mochila auricular cartucho parlantes procesador gpu placa madre";
	archivo.close();
}

void leer_archivo(int lon, const char *text) {
	ifstream archivo;
	string texto;
	archivo.open("prueba.txt", ios::in);

	if (archivo.fail()) {
		cout << "No se puede abrir archivo";
		exit(1);
	}
	while (!archivo.eof()) {
		getline(archivo, texto);
	}
	archivo.close();
	text = texto.c_str();
	lon = texto.length();
	longitud = lon;
	//cout << text << endl;
	//cout << "Su longitud es: " << lon << endl;
}
void boyer_moore() {
	system("CLS");
	cout << "*******Boyer_Moore*****" << endl;
	escribir();
	int lon = 0;
	const char *text;
//leer_archivo(lon,*text);
	char patron[100];
	int M = 0;
	/*
	 cout << "Ingrese el texto a analizar : ";
	 cin.ignore();
	 cin.getline(text, 100, '\n');
	 N = strlen(text);
	 */
	//abrir archivo
	ifstream archivo;
	string texto;
	archivo.open("prueba.txt", ios::in);

	if (archivo.fail()) {
		cout << "No se puede abrir archivo";
		exit(1);
	}
	while (!archivo.eof()) {
		getline(archivo, texto);
	}
	archivo.close();
	text = texto.c_str();
	lon = texto.length();
	longitud = lon;
	//cout << text << endl;
	//cout << "Su longitud es: " << lon << endl;
	//finaliza leer archivo


	do {
		cout << "\nIngrese lo que desea buscar : ";
		cin >> patron;
		M = strlen(patron);
		if (M > longitud)
			cout << "\t\t ERROR!!! Texto > Patron... \n";
	} while (M > longitud);
	auto inicio = high_resolution_clock::now();
	string cadenaTexto(text);
	boyer_moore_productos(cadenaTexto, patron, M, longitud);
	auto fin = high_resolution_clock::now();
	cout << endl;
	auto duracion = duration_cast<microseconds>(fin - inicio);
	cout << "el tiempo de ejecucion fue: " << duracion.count()
			<< " microsegundos" << endl;
}

