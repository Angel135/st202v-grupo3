/*
 * Heapsort.cpp
 *
 *  Created on: 15 abr. 2018
 *      Author: Lau
 */
#include <iostream>
#include <chrono>
#include <stdlib.h>
#include "../Utilitarios/Utilitarios.hpp"
using namespace std;
using namespace std::chrono;

void heapify(producto p[],int cant,int i,int tipo,int orden) {
	//ordenar subarbol con raiz indice "i"
	//tipo  0 : por cantidad
	//      1 : por precio
	//orden 0 : ascendente
	//		1 : descendente

	int padre=i;
	int izq=2*i+1;
	int der=2*i+2;

	if (tipo==0) {

		if (orden==0) {

			if (izq<cant && p[izq].cantidad>p[padre].cantidad) {
				padre=izq;
			}

			if (der<cant && p[der].cantidad>p[padre].cantidad) {
				padre=der;
			}

			if (padre!=i) {
				swap(p[padre],p[i]);
				heapify(p,cant,padre,tipo,orden);
			}
		}

		else if (orden==1) {

			if (izq<cant && p[izq].cantidad<p[padre].cantidad) {
				padre=izq;
			}

			if (der<cant && p[der].cantidad<p[padre].cantidad) {
				padre=der;
			}

			if (padre!=i) {
				swap(p[padre],p[i]);
				heapify(p,cant,padre,tipo,orden);
			}
		}
	}

	else if (tipo==1) {

		if (orden==0) {
			if (izq<cant && p[izq].precio>p[padre].precio) {
				padre=izq;
			}

			if (der<cant && p[der].precio>p[padre].precio) {
				padre=der;
			}

			if (padre!=i) {
				swap(p[padre],p[i]);
				heapify(p,cant,padre,tipo,orden);
			}
		}

		else if (orden==1) {

			if (izq<cant && p[izq].precio<p[padre].precio) {
				padre=izq;
			}

			if (der<cant && p[der].precio<p[padre].precio) {
				padre=der;
			}

			if (padre!=i) {
				swap(p[padre],p[i]);
				heapify(p,cant,padre,tipo,orden);
			}
		}
	}
}

void heapsort(producto p[],int cant,int tipo,int orden) {
	//tipo	0 : por cantidad
	//		1 : por precio
	//orden 0 : ascendente
	//		1 : descendente

	for (int i=cant/2-1;i>=0;i--) {
		heapify(p,cant,i,tipo,orden);
	}

	for (int i=cant-1;i>=0;i--) {
		swap(p[0],p[i]);
		heapify(p,i,0,tipo,orden);
	}

}
