/*
 * Heapsort.hpp
 *
 *  Created on: 15 abr. 2018
 *      Author: Bryan
 */

#ifndef HEAPSORT_HEAPSORT_HPP_
#define HEAPSORT_HEAPSORT_HPP_

#include "../Utilitarios/Utilitarios.hpp"

void heapsort(producto arreglo[],int cant,int orden,int tipo);
//tipo  0 : por cantidad
//	    1 : por precio
//orden 0 : ascendente
//		1 : descendente


#endif /* HEAPSORT_HEAPSORT_HPP_ */
