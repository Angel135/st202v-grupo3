/*
 * Criba_Eratostenes.cpp
 *
 *  Created on: 15 abr. 2018
 *      Author: Bryan
 */

#include <iostream>
#include <chrono>
#include <vector>
#include <cstdlib>
#include <stdlib.h>
#include <cmath>

using namespace std;
using namespace std::chrono;

void criba(int n){
	vector<bool> primo;
	bool aux=false;
		for (int i=0;i<n;i++){
			primo.push_back(aux);
		}
		for (int i=0;i<n;i++){
			primo[i]=true;
		}
		for(int i=2;i<pow(n,0.5);i++){
			if(primo[i]){
			for(int j=2;i*j<n;j++){
				primo[i*j]=false;
				}
			}
		}
		for(int i=2;i<n;i++){
			if(primo[i])cout<<i<< ' ';
		}
		cout<<endl;

}



	void inicio_criba(){
		system("CLS");
		cout<<"La criba de Eratostenes"<<endl;
		int a;
		cout<<endl;

		do{
		cout<<"Ingrese el numero :";
		cin>>a;
			auto inicio = high_resolution_clock::now();
		criba(a);
		cout<<endl;
			auto fin = high_resolution_clock::now();
		//	obtiene hora de salida del algoritmo

			auto duracion = duration_cast<microseconds>(fin-inicio);
		//	obtiene la duracion

			cout<<"el tiempo de ejecucion fue: "<<duracion.count()<<" microsegundos"<<endl;
		//	el tiempo de ejecucion
		}while(a!=0);
	system("pause");
	}
