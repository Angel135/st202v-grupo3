//============================================================================
// Name        : Proyecto.cpp
// Author      : Grupo 03
// Version     : 3.22
// Copyright   : Copyright ...
// Description : Proyecto
//============================================================================

#include <iostream>
#include "Al_Prim/Al_Prim.hpp"
#include "Boyer_Moore/Boyer_Moore.hpp"
#include "Criba_Eratostenes/Criba_Eratostenes.hpp"
#include "Heapsort/Heapsort.hpp"
#include "Utilitarios/Utilitarios.hpp"
#include <cstring>
#include <cstdlib>
#include <chrono>
#include <stdlib.h>
#include <fstream>

using namespace std;
using namespace std::chrono;


//vendedor
int cant_vendedor=5;
struct form_vendedor vendedor[100] ={	{"Pedro","Abad",315784,76909715,19890327,"Av. Independencia NO. 241",933945721,"pedroabad@gmail.com",204567849,123456},
										{"Sandra","Camberos",842325,76809616,19880219,"Av. Independencia NO. 240",923946723,"sandracamberos@hotmail.com",204567849,234567},
										{"Leandro","Ganboa",327386,75904715,19900131,"Av. Independencia NO. 201",982945727,"leandro33@yahoo.com",204567849,345678},
										{"Luis","Morales",746573,76809925,19840821,"Av. Independencia NO. 204",923949724,"luis22morales@gmail.com",204567849,456789},
										{"Ema","Villacriz",547997,76609785,19801214,"Av. Independencia NO. 211",993915628,"emmmav@gmail.com",204567849,567891},
										{"Eugenia","Fabela",678910,73909718,19960504,"Av. Independencia NO. 261",933545724,"eug_fabela@gmail.com",204567849,678910},
};

//cliente
int cant_cliente=5;
struct form_cliente	cliente[100] =	{	{"Edgard","Ramos",306789,76909705,19890327,"Av. Independencia NO. 260",933945721,"edgardramos@gmail.com",123456},
										{"Evelyn","Perez",742324,76809616,19880219,"Av. Independencia NO. 253",923946723,"evelynperez@hotmail.com",234567},
										{"Maricruz","Soto",317356,75904715,19900131,"Av. Independencia NO. 226",982945727,"marisoto@yahoo.com",345678},
										{"Deysi","Moreno",746573,76809925,19940821,"Av. Independencia NO. 281",923949724,"deysim@gmail.com",456789},
										{"Eduardo","Orjeda",547997,76609785,19901214,"Av. Independencia NO. 291",993915628,"edu22@gmail.com",567891},
										{"Jesus","Bustamante",678910,73609718,16960504,"Av. Independencia NO. 253",933545724,"jesusbus@gmail.com",678910},
};

//producto
int cant_producto=10;
struct producto p[]={					{"monitor",10,750},
										{"mouse/teclado",25,100},
										{"disco duro",6,80},
										{"laptop",4,1799},
										{"mochila",1,180},
										{"auricular",4,90},
										{"cartucho",14,50},
										{"parlantes",5,150},
										{"procesador",10,800},
										{"gpu",5,1400},
};

struct producto b[]={};

void proyecto(struct form_vendedor vendedor[],struct form_cliente cliente[],struct producto p[],struct producto b[],int &cant_vendedor,int &cant_cliente,int cant_producto) {
	int op,id_new,psw_new,x=0,y;

	//primer menu 1
	system("cls");
	cout<<"*******ELECTROEXPRESS*******"<<endl;
	cout<<"<1> Compra con Nosostros"<<endl;
	cout<<"<2> Vende con Nosostros"<<endl;
	cout<<"Introducir Opcion: ";cin>>op;
	system("cls");

	if (op==1) {

		//segundo menu 1.1
		op=0;
		cout<<"**********COMPRAR***********"<<endl;
		cout<<"<1> Ingresar"<<endl;
		cout<<"<2> Registrar"<<endl;
		cout<<"Introducir Opcion: ";cin>>op;
		system("cls");

		//ingreso cliente
		if (op==1) {

			//tercer menu 1.1.1
			op=0;
			cout<<"******INGRESAR CLIENTE*******"<<endl;
			cout<<"Id: ";cin>>id_new;
			cout<<"Psw: ";cin>>psw_new;

			//verificar id y psw (password)
			while (x==0) {
				for (int i=0;i<cant_cliente;i++) {
					if (cliente[i].id==id_new && cliente[i].psw==psw_new) {
						system("cls");
						cout<<"Logeando..."<<endl;
						x=1;
					}
				}
			}

			switch (x) {

				case 0: system("cls");
						//cuarto menu 1.1.1.0
						cout<<"Id y/o Psw incorrecto(s)"<<endl;
						break;

				case 1: system("cls");
						//cuarto menu 1.1.1.1
						op=0;
						cout<<"*******ELECTROEXPRESS*******"<<endl;
						cout<<"ID: "<<id_new<<endl;
						cout<<"<1> Buscar"<<endl;
						cout<<"<2> Mostrar Ordenado"<<endl;
						cout<<"<3> Mostrar los Productos"<<endl;
						cout<<"Introducir Opcion: ";cin>>op;

						switch (op) {

							case 1:	system("cls");
									//quinto menu 1.1.1.1.1
									cout<<"***********BUSCAR***********"<<endl;
									//algoritmo boyer moore!---------------------------------------------------------------
									//-------------------------------------------------------------------------------------
									boyer_moore_proy (p,b,cant_producto);


									break;

							case 2:	system("cls");
									//quinto menu 1.1.1.1.2
									op=0;
									cout<<"**********ORDENAR***********"<<endl;
									cout<<"<1> Por Precio"<<endl;
									cout<<"<2> Por Cantidad"<<endl;
									cout<<"Introducir Opcion: ";cin>>op;

									switch (op) {

										case 1:	system("cls");
												//sexto menu 1.1.1.1.1
												op=0;
												cout<<"*********POR PRECIO*********"<<endl;
												cout<<"<1> Ascendente"<<endl;
												cout<<"<2> Descendente"<<endl;
												cout<<"Introducir Opcion: ";cin>>op;

												switch (op) {
													case 1:	heapsort(p,cant_producto,op-1,1);
															break;
													case 2:	heapsort(p,cant_producto,op-1,1);
															break;
												}

												system("cls");
												//menu productos ordenados por precio
												cout<<"******************PRODUCTOS*****************"<<endl;
												mostrar(p,cant_producto);
												system("PAUSE");
												break;

										case 2:	system("cls");

												//sexto menu 1.1.1.1.1.2
												op=0;
												cout<<"********POR CANTIDAD********"<<endl;
												cout<<"<1> Ascendente"<<endl;
												cout<<"<2> Descendente"<<endl;
												cout<<"Introducir Opcion: ";cin>>op;

												switch (op) {
													case 1:	heapsort(p,cant_producto,op-1,0);
															break;
													case 2:	heapsort(p,cant_producto,op-1,0);
															break;
												}

												system("cls");
												//menu productos ordenados por cantidad
												cout<<"******************PRODUCTOS*****************"<<endl;
												mostrar(p,cant_producto);
												system("PAUSE");
												break;
									}
									break;

							case 3:	system("cls");
									cout<<"******************PRODUCTOS*****************"<<endl;
									mostrar(p,cant_producto);
									system("PAUSE");
									break;
						}

						break;
			}

			system("cls");
		}

		//registo cliente
		else if (op==2) {
			op=0;
			struct form_cliente aux1;

			cout<<"*****REGISTRAR CLIENTE******"<<endl;
			cout<<"Nombre: ";cin>>aux1.nombre;
			cout<<"Apellido: ";cin>>aux1.apellido;
			cout<<"ID: ";cin>>aux1.id;
			cout<<"DNI: ";cin>>aux1.dni;
			cout<<"Fecha de Nac.: ";cin>>aux1.nac;
			cout<<"Direccion: ";cin>>aux1.direccion;
			cout<<"Celular: ";cin>>aux1.celular;
			cout<<"Email: ";cin>>aux1.email;
			cout<<"Contrasenia generada: "<<contrasena(aux1.dni,aux1.celular);
			//algoritmo de prim! -----------------------------------------------------------------
			//------------------------------------------------------------------------------------

			y=verificarcliente(cliente,aux1,cant_cliente);

			if (y==0) {
				cout<<"Registrado con Exito"<<endl;
				cant_cliente++;
				cliente[cant_cliente]=aux1;
			}

			else if (y==1){
				cout<<"Error en el Registro"<<endl;
			}
		}
	}

	//vende con nosotros
	if (op==2) {

		//segundo menu 1.1
		op=0;
		cout<<"**********VENDER***********"<<endl;
		cout<<"<1> Ingresar"<<endl;
		cout<<"<2> Registrar"<<endl;
		cout<<"Introducir Opcion: ";cin>>op;
		system("cls");

		//ingreso vendedor
		if (op==1) {

			//tercer menu 1.1.1
			op=0;
			cout<<"*****INGRESAR VENDEDOR******"<<endl;
			cout<<"Id: ";cin>>id_new;
			cout<<"Psw: ";cin>>psw_new;

			//verificar id y psw (password)
			while (x==0) {
				for (int i=0;i<cant_cliente;i++) {
					if (vendedor[i].id==id_new && vendedor[i].psw==psw_new) {
						system("cls");
						cout<<"Logeando..."<<endl;
						x=1;
					}
				}
			}

			switch (x) {

				case 0: system("cls");
						//cuarto menu 1.1.1.0
						cout<<"Id y/o Psw incorrecto(s)"<<endl;
						break;

				case 1: system("cls");
						//cuarto menu 1.1.1.1
						op=0;
						cout<<"*******ELECTROEXPRESS*******"<<endl;
						cout<<"ID: "<<id_new<<endl;
						cout<<"<1> Buscar"<<endl;
						cout<<"<2> Mostrar Ordenado"<<endl;
						cout<<"<3> Mostrar los Productos"<<endl;
						cout<<"Introducir Opcion: ";cin>>op;

						switch (op) {

							case 1:	system("cls");
									//quinto menu 1.1.1.1.1
									cout<<"***********BUSCAR***********"<<endl;
									cout<<"Ingrese la palabra a buscar:"<<endl;

									//algoritmo boyer moore!---------------------------------------------------------------
									//-------------------------------------------------------------------------------------
									boyer_moore_proy (p,b,cant_producto);

									break;

							case 2:	system("cls");
									//quinto menu 1.1.1.1.2
									op=0;
									cout<<"**********ORDENAR***********"<<endl;
									cout<<"<1> Por Precio"<<endl;
									cout<<"<2> Por Cantidad"<<endl;
									cout<<"Introducir Opcion: ";cin>>op;

									switch (op) {

										case 1:	system("cls");
												//sexto menu 1.1.1.1.1
												op=0;
												cout<<"*********POR PRECIO*********"<<endl;
												cout<<"<1> Ascendente"<<endl;
												cout<<"<2> Descendente"<<endl;
												cout<<"Introducir Opcion: ";cin>>op;

												switch (op) {
													case 1:	heapsort(p,cant_producto,op-1,1);
															break;
													case 2:	heapsort(p,cant_producto,op-1,1);
															break;
												}

												system("cls");
												//menu productos ordenados por precio
												cout<<"******************PRODUCTOS*****************"<<endl;
												mostrar(p,cant_producto);
												break;

										case 2:	system("cls");

												//sexto menu 1.1.1.1.1.2
												op=0;
												cout<<"********POR CANTIDAD********"<<endl;
												cout<<"<1> Ascendente"<<endl;
												cout<<"<2> Descendente"<<endl;
												cout<<"Introducir Opcion: ";cin>>op;

												switch (op) {
													case 1:	heapsort(p,cant_producto,op-1,0);
															break;
													case 2:	heapsort(p,cant_producto,op-1,0);
															break;
												}

												system("cls");
												//menu productos ordenados por cantidad
												cout<<"******************PRODUCTOS*****************"<<endl;
												mostrar(p,cant_producto);
												break;
									}
									break;

							case 3:	cout<<"******************PRODUCTOS*****************"<<endl;
									mostrar(p,cant_producto);
									break;
						}

						break;
			}
		}

		//registo vendedor
		else if (op==2) {
			op=0;
			struct form_vendedor aux2;

			cout<<"*****REGISTRAR VENDEDOR******"<<endl;
			cout<<"Nombre: ";cin>>aux2.nombre;
			cout<<"Apellido: ";cin>>aux2.apellido;
			cout<<"ID: ";cin>>aux2.id;
			cout<<"DNI: ";cin>>aux2.dni;
			cout<<"Fecha de Nac.: ";cin>>aux2.nac;
			cout<<"Direccion: ";cin>>aux2.direccion;
			cout<<"Celular: ";cin>>aux2.celular;
			cout<<"Email: ";cin>>aux2.email;
			cout<<"RUC: ";cin>>aux2.ruc;
			cout<<"Contrasenia generada: "<<contrasena(aux2.dni,aux2.celular);
			//algoritmo de prim! -----------------------------------------------------------------
			//------------------------------------------------------------------------------------

			y=verificarvendedor(vendedor,aux2,cant_vendedor);

			if (y==0) {
				cout<<"Registrado con Exito"<<endl;
				cant_vendedor++;
				vendedor[cant_vendedor]=aux2;
			}

			else if (y==1){
				cout<<"Error en el Registro"<<endl;
			}
		}
	}
}

int main() {
	cout<<"**********GRUPO 03**********"<<endl;
	int op;
	cout<<"<1> Algoritmos"<<endl;
	cout<<"<2> Proyecto"<<endl;
	cout<<"<3> SALIR"<<endl;
	cout<<"Introducir Opcion: ";
	cin>>op;
	
	switch(op){

		case 1:
			algoritmos();
		break;

		case 2:
			proyecto(vendedor,cliente,p,b,cant_vendedor,cant_cliente,cant_producto);
		break;

		default:
			exit(0);
	}

    system("PAUSE");
	return 0;
}


